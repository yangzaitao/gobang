import { _decorator, Component } from 'cc';
import { loading, SCENE } from './loading';
const { ccclass } = _decorator;

@ccclass('main')
export class main extends Component {
    onload() {
        loading.load(SCENE.MAIN);
    }
}
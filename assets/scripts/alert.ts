import { _decorator, Component, Label, Node } from 'cc';
import { loading, SCENE } from './loading';
const { ccclass, property } = _decorator;

@ccclass('alert')
export class alert extends Component {
    @property(Label)
    title: Label;
    static I: alert;
    protected start(): void {
        alert.I = this;
        this.node.active = false;
    }
    static callfun: () => void = function () { };
    static onToggle(title: string, show: boolean = true) {
        this.I.node.active = show;
        this.I.title.getComponent(Label).string = title;
    }
    clear() {
        loading.load(SCENE.HOME);
    }
    continue() {
        alert.callfun();
        alert.I.node.active = false;
    }
}


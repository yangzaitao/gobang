import { _decorator, Component, director, ProgressBar } from 'cc';
const { ccclass, property } = _decorator;
enum SCENE {
    MAIN = 'main',
    HOME = 'home',
    LOADING = 'loading'
}

@ccclass('loading')
export class loading extends Component {
    @property(ProgressBar) //ide面板挂载bar组件拖放
    bar: ProgressBar;
    static scene: SCENE = SCENE.MAIN;
    static load(scene: SCENE) {
        this.scene = scene;
        director.loadScene(SCENE.LOADING);
    }
    open() {
        director.loadScene(loading.scene);
    }
    update(dt: number): void {
        if (this.bar.progress < 1) {
            this.bar.progress += 0.5 * dt;  //丝滑更新进度条
            if (this.bar.progress >= 1) {   //进度条值大1后执行open
                this.open();
            }
        }
    }
}

export { SCENE };
import { _decorator, Component, director, Layout, Node, Prefab } from 'cc';
import { Control } from './control';
const { ccclass, property, type } = _decorator;

@ccclass('main')
export class main extends Component {
    @property(Layout)
    layout: Layout;
    @property(Prefab)
    gridbg: Prefab;
    @property(Prefab)
    white: Prefab;
    @property(Prefab)
    black: Prefab;
    start() {
        Control.gridbg = this.gridbg;
        Control.white = this.white;
        Control.black = this.black;
        Control.layout = this.layout;
        Control.init(Control.layout);
    }
}


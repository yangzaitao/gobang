import { Layout, Node, NodeEventType, Prefab, UITransform, instantiate } from "cc";
import { playaudios } from "./lib/audio";
import { alert } from "./alert";
class Control {
    static layout: Layout;
    static iswhite: boolean = false;
    static size: number = 5;
    static girdsize: number = 15;
    static girds: Node[] = [];              //默认的棋盘元素
    static girdss: Node[][] = [];           //格式化后的棋盘元素（用于判断输赢）
    static gridbg: Prefab;                  //棋盘背景
    static black: Prefab;                   //黑棋
    static white: Prefab;                   //白棋
    static ondown(ent: { target: Node }) {  //制定一个一个点击监听事件
        const self = ent.target;
        if (self.children.length === 0) {   //判断当前girdbg节点是否不是0
            let pawn: Node = null;
            if (this.iswhite) {
                pawn = instantiate(this.white);
            } else {
                pawn = instantiate(this.black);
            }
            self.name = pawn.name;          //标记是黑是白
            self.addChild(pawn);
            playaudios();                   //下棋音
            this.iswhite = !this.iswhite;   //取反（实现黑白棋换着出棋
            this.showGrid();
            if (this.isWin(ent.target)) {
                alert.callfun = () => this.init(this.layout);
                alert.onToggle(`${ent.target.name}胜利!`);
            }
        }
    };
    static showGrid() {
        const [, black, white] = this.layout.node.parent.children;
        white.children[0].active = this.iswhite;
        black.children[0].active = !this.iswhite;
    }
    static init(layout: Layout) {           //初始化棋盘
        if (layout !== undefined) {
            layout.node.removeAllChildren();//清空子元素（为重新开始初始化做铺垫）
            const wh = layout.getComponent(UITransform);
            for (let i = 0; i < this.girdsize; i++) {
                for (let n = 0; n < this.girdsize; n++) {
                    const item = instantiate(this.gridbg);
                    const uiTransform = item.getComponent(UITransform);
                    uiTransform.contentSize.set(wh.width / this.girdsize, wh.width / this.girdsize);
                    item.name = `x${n}-y${i}`;
                    item.on(NodeEventType.TOUCH_START, this.ondown, Control);
                    layout.node.addChild(item);
                }
            }
            Control.girds = layout.node.children;
            this.girdss = [];               //清空
            for (let i = this.girdsize; i <= this.girds.length; i += this.girdsize) {
                this.girdss.push(this.girds.slice(i - this.girdsize, i));
            }
            this.showGrid();
        }
    };
    static isWin(target: Node): boolean {
        let post = { x: 0, y: 0 };
        //在girdss找到target的xy位置
        for (let y = 0; y < this.girdss.length; y++) {
            const x = this.girdss[y].indexOf(target);
            if (x !== -1) {
                post.x = x;
                post.y = y;
                break;
            }
        }
        let lr: Node[] = [],
            tb: Node[] = [],
            ltrb: Node[] = [],
            rtlb: Node[] = [];
        for (let i = 0; i < this.size; i++) {
            const lft = post.x - i;
            const rgt = post.x + i;
            const top = post.y - i;
            const btm = post.y + i;
            const arrs = this.girdss;
            //添加所有的x节点
            if (lft >= 0) {
                lr.unshift(arrs[post.y][lft]);
            }
            if (rgt < (this.girdsize - 1)) {
                lr.push(arrs[post.y][rgt + 1]);
            }
            //添加所有的y节点
            if (top >= 0) {
                tb.unshift(arrs[top][post.x]);
            }
            if (btm < this.girdsize) {
                if (arrs[btm + 1] !== undefined)
                    tb.push(arrs[btm + 1][post.x]);
            }
            //添加所有ltrb节点
            if (lft >= 0 && top >= 0) {
                ltrb.unshift(arrs[top][lft]);
            }
            if (rgt < this.girdsize && btm < this.girdsize) {
                if ((arrs[btm + 1] !== undefined) && (arrs[btm + 1][rgt + 1] !== undefined))
                    ltrb.push(arrs[btm + 1][rgt + 1]);
            }
            //添加所有rtlb节点
            if (rgt < this.girdsize && top >= 0) {
                rtlb.unshift(arrs[top][rgt])
            }
            if (lft > 0 && btm < this.girdsize) {
                if ((arrs[btm + 1] !== undefined) && (arrs[btm + 1][lft - 1] != undefined))
                    rtlb.push(arrs[btm + 1][lft - 1]);
            }
        }
        return function (): boolean {
            const bol = [lr, tb, ltrb, rtlb].some((pos) => {
                let arr = [];
                for (let i = 0; i < pos.length; i++) {
                    if (pos[i].name === target.name) {
                        arr.push(pos[i]);
                    } else {
                        if (arr.length < Control.size) {
                            arr = [];
                        }
                    }
                }
                if (arr.length === Control.size) {
                    // console.log('yzt', arr);
                }
                return arr.length >= Control.size;
            });
            return bol;
        }();
    }
    static clear() {

    }
}

export { Control };
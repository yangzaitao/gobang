import { AudioClip, AudioSource, resources } from 'cc';
function playaudios() {
    //加载并且播放下棋的声音
    resources.load('audio/chess', (err, audio: AudioClip) => {
        if (!err) {
            const audio_ = new AudioSource();
            audio_.playOneShot(audio, 1);
        }
    });
}

export { playaudios };